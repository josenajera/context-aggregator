from bs4 import BeautifulSoup
import requests
from inflector import Inflector, English
import re
import json

stopwords = set([
        "i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself",
        "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself",
        "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that",
        "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had",
        "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as",
        "until", "while", "of", "at", "by", "for", "with", "about", "against", "between", "into", "through",
        "during", "before", "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off",
        "over", "under", "again", "further", "then", "once", "here", "there", "when", "where", "why", "how",
        "all", "any", "both", "each", "few", "more", "most", "other", "some", "such", "no", "nor", "not",
        "only", "own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just", "don", "should",
        "now"])

def get_page_soup(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    page = requests.get(url, headers=headers)
    soup = BeautifulSoup(page.content, "html.parser")
    return soup

def extract_article_metadata(url):
    keys = ['title', 'image', 'url', 'description']
    soup = get_page_soup(url)
    meta = {}
    for tag in soup.find_all("meta"):
        tname = tag.get('property')
        if tname == None:
            continue
        if 'og:' in tname:
            tname = tname[tname.index('og:')+3:]
        if tname in keys:
            meta[tname] = tag.get('content', None)
    return meta

def sanitize_text(text, stopwords):
    rpatterns = ["\'s", "\'m", "\'ve", "\'d", "\'"]
    # replace this loop with a comprehension
    for c in rpatterns:
        text = text.replace(c, '')
    text = re.sub('[,.;:\|\+\'&!@#$£?0-9]', '', text)
    return set(text.lower().split(' ')).difference(stopwords).difference(set(['', '|', '-']))

def singularize_set(inflector, words):
    return set([inflector.singularize(word) for word in words])

def filter_unrelated_articles(unfiltered_articles, keywords, match_set, stopwords):
    inflector = Inflector(English)
    keywords = singularize_set(inflector, sanitize_text(keywords, stopwords))
    articles = []
    for ua in unfiltered_articles:
        match_found = False
        match_results = {}
        for match_field in match_set:
            sfield = singularize_set(inflector, sanitize_text(ua.get(match_field, ' '), stopwords))
            match = keywords.intersection(sfield)
            match_results[match_field] = list(match)
            if len(match) > 0:
                match_found = True
        if match_found:
            ua['matches'] = match_results
            articles.append(ua)
    return articles

def extract_article_headers(keywords, source, n = 20):
    search_url = 'https://www.google.com/search?q='
    url = search_url + ('site:' + source + ' ' + keywords).replace(' ', '+') + '&num=' + str(n)
    # print(url)
    soup = get_page_soup(url)

    article_headers = soup('div', 'g')
    articles = []
    for a in article_headers:
        try:
            href = a.find('a')['href']
            # href = href[href.index('http'):href.index('&')]
            # print(href)
            meta = extract_article_metadata(href)
            articles.append(meta)
            # print(json.dumps(meta, indent=4, ensure_ascii=False))
            # TODO extract timestamp (from meta?)
        except Exception as error:
            # print(error)
            continue
    # print(json.dumps(articles, indent=4, ensure_ascii=False))
    articles = filter_unrelated_articles(articles, keywords, ['title', 'description'], stopwords)
    return { 'query': keywords, 'url-query': url, 'source': source, 'articles': articles }

#----------------------------------------------------------------------------------
if __name__ == '__main__':

    keywords = 'Meghan Markle shoes'
    sources = ['www.mirror.co.uk', 'www.theguardian.com', 'www.bbc.co.uk', 'www.google.com']
    num = 5
    headers = []
    for source in sources:
        headers.append(extract_article_headers(keywords, source, num))

    # print(headers)
    print(json.dumps(headers, indent=4, ensure_ascii=False))