from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By

from selenium.webdriver.support.ui import WebDriverWait
import sys
import time
import re

from inflector import Inflector, English

stopwords = set([
        "i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself",
        "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself",
        "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that",
        "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had",
        "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as",
        "until", "while", "of", "at", "by", "for", "with", "about", "against", "between", "into", "through",
        "during", "before", "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off",
        "over", "under", "again", "further", "then", "once", "here", "there", "when", "where", "why", "how",
        "all", "any", "both", "each", "few", "more", "most", "other", "some", "such", "no", "nor", "not",
        "only", "own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just", "don", "should",
        "now"])


def extract_article_headers(handler):
    article_headers = handler.find_elements(By.CLASS_NAME, value='g')
    articles = []
    for a in article_headers:
        # Skip images entry
        if a.get_attribute('id') == 'imagebox_bigimages' or a.get_attribute('class') != 'g':
            continue
        title = a.find_element(By.TAG_NAME, value='h3')
        href = a.find_element(By.TAG_NAME, value='a').get_attribute('href')
        body = a.find_element(By.CLASS_NAME, value='st')
        ts = body.find_elements(By.CLASS_NAME, value='f')
        abstract = body.text
        if not ts:
            ts = 'no-timestamp'
        else:
            abstract = abstract[len(ts[0].text)+1:]
            ts = (ts[0].text).rstrip(' -')
        articles.append((title.text, href, ts, abstract))
    return articles

def sanitize_text(text, stopwords):
    rpatterns = ["\'s", "\'m", "\'ve", "\'d", "\'"]
    # replace this loop with a comprehension
    for c in rpatterns:
        text = text.replace(c, '')
    text = re.sub('[,.;:\|\+’&!@#$£0-9]', '', text)
    return set(text.lower().split(' ')).difference(stopwords).difference(set(['', '|', '-']))

def singularize_set(inflector, words):
    return set([inflector.singularize(word) for word in words])

def filter_unrelated_articles(unfiltered_articles, keywords, stopwords):
    inflector = Inflector(English)
    keywords = singularize_set(inflector, sanitize_text(keywords, stopwords))
    print(keywords)
    articles = []
    for (title, href, ts, abstract) in unfiltered_articles:
        stitle = singularize_set(inflector, sanitize_text(title, stopwords))
        title_match = keywords.intersection(stitle)
        # print('-'*80)
        # print(keywords)
        # print(stitle)
        # print(title_match)
        if title_match:
            articles.append((title, list(title_match), href, ts, abstract))
    return articles

    set([inflector.singularize(word) for word in words])

#----------------------------------------------------------------------------------
if __name__ == '__main__':


    keywords = ''
    for i in range(1, len(sys.argv), 1):
        keywords += sys.argv[i] + ' '

    search_url = 'https://www.google.com/search?q='
    #query = 'articles UK newspapers'
    source = 'site:mirror.co.uk'
    # source = 'site:https://www.theguardian.com'
    # source = 'site:bbc.co.uk'
    nresults = '&num=20'

    driver = webdriver.Chrome()

    url = search_url + (source + ' ' + keywords).replace(' ', '+') + nresults
    print(url)
    print('QUERY: ' + keywords)

    driver.get(url)

    articles = extract_article_headers(driver)
    articles = filter_unrelated_articles(articles, keywords, stopwords)

    for a in articles:
        print(a)

    driver.close()

