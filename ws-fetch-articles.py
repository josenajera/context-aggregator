from flask import Flask, request
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('key', action='append', required=True, help='You must specify a list of keywords')
parser.add_argument('src', action='append')
parser.add_argument('n', type=int, help='Number of articles per source to fetch')

MAX_CRAWL = 10

class ArticleCrawler(Resource):
    """
    You can try this example as follow:
        $ curl http://localhost:5000/todo1 -d "data=Remember the milk" -X PUT
        $ curl http://localhost:5000/todo1
        {"todo1": "Remember the milk"}
        $ curl http://localhost:5000/todo2 -d "data=Change my breakpads" -X PUT
        $ curl http://localhost:5000/todo2
        {"todo2": "Change my breakpads"}

    Or from python if you have requests :
     >>> from requests import put, get
     >>> put('http://localhost:5000/todo1', data={'data': 'Remember the milk'}).json
     {u'todo1': u'Remember the milk'}
     >>> get('http://localhost:5000/todo1').json
     {u'todo1': u'Remember the milk'}
     >>> put('http://localhost:5000/todo2', data={'data': 'Change my breakpads'}).json
     {u'todo2': u'Change my breakpads'}
     >>> get('http://localhost:5000/todo2').json
     {u'todo2': u'Change my breakpads'}

    """
    def get(self):
        sources = set()
        sources.add('www.google.com')
        # curl http: // localhost: 5000 / fetch - d "key=Meghan&key=Markle&key=shoes" -X GET
        args = parser.parse_args()
        keys = args['key']
        n = MAX_CRAWL if args['n'] is None else args['n']
        sources = [ src for src in sources.union({ x for x in args['src'] })]
        # TODO Use list of keywords and sources to call article crawler
        return { 'keys': keys, 'fetch': n, 'sources': sources }

    def put(self):
        args = parser.parse_args()
        query = args['query']
        print(query)
        return query

api.add_resource(ArticleCrawler, '/fetch')

if __name__ == '__main__':
    app.run(debug=False)
