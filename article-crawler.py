import requests
from lxml import html
from bs4 import BeautifulSoup
from inflector import Inflector, English
import json
import sys
import re

stopwords = set([
        "i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself",
        "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself",
        "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that",
        "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had",
        "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as",
        "until", "while", "of", "at", "by", "for", "with", "about", "against", "between", "into", "through",
        "during", "before", "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off",
        "over", "under", "again", "further", "then", "once", "here", "there", "when", "where", "why", "how",
        "all", "any", "both", "each", "few", "more", "most", "other", "some", "such", "no", "nor", "not",
        "only", "own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just", "don", "should",
        "now"])

def sanitize_text(text, stopwords):
    rpatterns = ["\'s", "\'m", "\'ve", "\'d", "\'"]
    # replace this loop with a comprehension
    for c in rpatterns:
        text = text.replace(c, '')
    text = re.sub('[,.;:\|\+\'&!@#$£?0-9]', '', text)
    return set(text.lower().split(' ')).difference(stopwords).difference(set(['', '|', '-']))

def singularize_set(inflector, words):
    return set([inflector.singularize(word) for word in words])

def filter_unrelated_articles(unfiltered_articles, keywords, stopwords):
    inflector = Inflector(English)
    keywords = singularize_set(inflector, sanitize_text(keywords, stopwords))
    articles = []
    for (title, href) in unfiltered_articles:
        stitle = singularize_set(inflector, sanitize_text(title, stopwords))
        title_match = keywords.intersection(stitle)
        if title_match:
            articles.append({'title': title, 'score': len(title_match)/len(keywords), 'match': list(title_match), 'url': href})
    return articles

def extract_article_headers(keywords, search_url, source, n = 20):
    url = search_url + ('site:' + source + ' ' + keywords).replace(' ', '+') + '&num=' + str(n)

    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")

    article_headers = soup('div', 'g')
    articles = []
    for a in article_headers:
        try:
            title = a.find('h3').text
            href  = a.find('a')['href']
            href = href[href.index('http'):href.index('&')]
            articles.append((title, href))
        # body  = a.find(class='st')
        # ts = body.find_elements(By.CLASS_NAME, value='f')
        # abstract = body.text
        # if not ts:
        #     ts = 'no-timestamp'
        # else:
        #     abstract = abstract[len(ts[0].text)+1:]
        #     ts = (ts[0].text).rstrip(' -')
        # articles.append((title.text, href, ts, abstract))
        except Exception:
            continue
    articles = filter_unrelated_articles(articles, keywords, stopwords)
    return { 'query': keywords, 'source': source, 'articles': articles }

#----------------------------------------------------------------------------------
if __name__ == '__main__':

    keywords = ''
    for i in range(1, len(sys.argv), 1):
        keywords += sys.argv[i] + ' '

    search_url = 'https://www.google.com/search?q='
    sources = ['www.google.com', 'www.mirror.co.uk', 'www.theguardian.com', 'www.bbc.co.uk']

    for source in sources:
        articles = extract_article_headers(keywords, search_url, source, 40)
        print(json.dumps(articles, indent=4, ensure_ascii=False))

